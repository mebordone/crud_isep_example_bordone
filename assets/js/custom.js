$(document).ready(function() {
    $('#product_table').DataTable({  
        language: {
                "lengthMenu": "Mostrar registros de _MENU_",
                "zeroRecords": "Nenhum resultado encontrado",
                "info": "Mostrando registros de _START_ a _END_ de um total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros de 0 a 0 de um total de 0 registros",
                "infoFiltered": "(filtrando um total de _MAX_ registros)",
                "sSearch": "Procurar:",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sLast":"Mais recentes",
                    "sNext":"Próximo",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Em processamento...",
            },      
        //para usar los botones   
        responsive: "true",
        dom: 'Bfrtilp',       
        buttons:[ 
			{
				extend:    'excelHtml5',
				text:      'xlsx',
				titleAttr: 'Exportar a Excel',
				className: 'btn btn-success'
			},
			{
				extend:    'pdfHtml5',
				text:      'pdf',
				titleAttr: 'Exportar a PDF',
				className: 'btn btn-danger'
			},
			{
				extend:    'print',
				text:      'print',
				titleAttr: 'Imprimir',
				className: 'btn btn-info'
			},
		]	        
    });
    $('#order_table').DataTable({        
        language: {
                "lengthMenu": "Mostrar registros de _MENU_",
                "zeroRecords": "Nenhum resultado encontrado",
                "info": "Mostrando registros de _START_ a _END_ de um total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros de 0 a 0 de um total de 0 registros",
                "infoFiltered": "(filtrando um total de _MAX_ registros)",
                "sSearch": "Procurar:",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sLast":"Mais recentes",
                    "sNext":"Próximo",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Em processamento...",
            }});
    $('#order_product_table').DataTable({        
        language: {
                "lengthMenu": "Mostrar registros de _MENU_",
                "zeroRecords": "Nenhum resultado encontrado",
                "info": "Mostrando registros de _START_ a _END_ de um total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros de 0 a 0 de um total de 0 registros",
                "infoFiltered": "(filtrando um total de _MAX_ registros)",
                "sSearch": "Procurar:",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sLast":"Mais recentes",
                    "sNext":"Próximo",
                    "sPrevious": "Anterior"
			     },
			     "sProcessing":"Em processamento...",
            }});
    $('#client_table').DataTable({        
        language: {
                "lengthMenu": "Mostrar registros de _MENU_",
                "zeroRecords": "Nenhum resultado encontrado",
                "info": "Mostrando registros de _START_ a _END_ de um total de _TOTAL_ registros",
                "infoEmpty": "Mostrando registros de 0 a 0 de um total de 0 registros",
                "infoFiltered": "(filtrando um total de _MAX_ registros)",
                "sSearch": "Procurar:",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sLast":"Mais recentes",
                    "sNext":"Próximo",
                    "sPrevious": "Anterior"
                    },
                    "sProcessing":"Em processamento...",
            }});
    $('#descricao').summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ],
        disableDragAndDrop: true
    });
    $('.delete-product').click(function () {
        var delete_url = $(this).data('id');
        $(".modal-footer #confirmDeleteProduct").attr("href", delete_url);
    });
    $('.delete-client').click(function () {
        var delete_url = $(this).data('id');
        $(".modal-footer #confirmDeleteClient").attr("href", delete_url);
    });
    $('.delete-order').click(function () {
        var delete_url = $(this).data('id');
        $(".modal-footer #confirmDeleteOrder").attr("href", delete_url);
    });
} );