##########################################
CodeIgniter + Bootstrap + MySQL CRUD
##########################################

This project are a CRUD of a simple product order system.

Install
*******************

- Have a dump in dump_database folder, you can import in your database;
- Configure the application/config/database.php to connect with your database;
- PHP version 5.6 or newer is recommended by CodeIgniter. I Have made this project in PHP 7.3;
- I Have utilized XAMPP for localy install.

Consigna de trabajo:
********************

PUNTO 1: Configurar entorno
Clonar el siguiente repositorio en tu local:
https://github.com/GFBarbosa/codeigniter-crud-example y realizar las configuraciones
necesarias para que funcione correctamente en tu computadora.

PUNTO 2: Desarrollar un ABM de Clientes
Actualmente solo hay un ABM de Productos y de Pedidos. Añadir la posibilidad de registrar
clientes. Cada uno tiene que tener los siguientes datos:
+ Nombre (*)
+ Apellido (*)
+ DNI (*)
+ Fecha de nacimiento
+ Provincia
+ Correo electrónico
(*) Estos campos son obligatorios.

PUNTO 3: Modificar Pedidos
Al contar con el registro de de Clientes, modificar el módulo de Pedidos para que se deba
indicar qué cliente está realizando dicho pedido.

PUNTO 4: Diseño
La aplicación tiene un diseño básico, sentite libre de mejorar los estilos para que sea más
atractiva y más amigable para el usuario.

PUNTO 5: Comportamiento
¿Se te ocurre una mejora? No dudes de aplicar javascript para llegar a un nuevo nivel.

Desarrollo:
********************

Día 1:

+ Entorno de produccion pc y notebook (lamp sobre manjaro, carga de archivos y base de datos)

Dia 2:

+ Creada tabla de clientes
+ Estructura de archivos para abm clientes

Dia 3:

+ Agregado ncliente a orden
+ Agregado favicon
+ Creacion de pedidos (home) en modo tabla paginada y buscador de productos
+ Creacion de pedidos - seleccionar clientes de una lista

Dia 4:

+ Mostrar informacion de clientes en vista ordenes
+ Link a datos completos y modificación del cliente

Dís 5:

+ Descarga de tablas de productos
+ Traduccion de las paginaciones de tablas
+ Agregado el editor en el proyecto para disponibilidad offline (corregir botones de editor)
+ Modificado model de productos (correccion de bug)
+ Msg helper muestra el mensaje una sola vez.

Bugs: No se observan

Mejoras: 

+ elegir de lista de provincias para clientes

Visualizacion de páginas
************************

Home
================================

.. image:: ./doc/Product-Order-CRUD_home.png
    :width: 400
    
Productos
================================

.. image:: ./doc/Product-Order-CRUD_produtos.png
    :width: 400

Pedidos
================================

.. image:: ./doc/Product-Order-CRUD_pedidos.png
    :width: 400

Clientes
================================

.. image:: ./doc/Product-Order-CRUD_clientes.png
    :width: 400
