<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-BR">
<?php $this->load->view('_partials/head'); ?>
<body>
<?php $this->load->view('_partials/header'); ?>
<div class="container container-person mt-5 p-5">
    <?=write_message()?>
    <?php
    $action_form = '/order/save/';
    if(isset($order) && $order){
        foreach ($order as $pedido);
        $action_form = $action_form.$pedido->id ?>
        <h1>Editar Pedido: <?= $pedido->id ?></h1>
    <?php 
    } else { ?>
        <h1>Novo Pedido</h1>
    <?php }
    if($products) { ?>
    <form id="form_make_order" method="post" action="<?=site_url($action_form)?>">
        <table id="order_product_table" class="table table-striped table-bordered table-responsive-sm" style="width:100%">
        <thead>
        <tr>
            <th><b>Nome</b></th>
            <th><b>SKU</b></th>
            <th><b>Preço</b></th>
            <th><b>Quantidade</b></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($products as $product) { ?>
            <tr>
                <td><?= $product->nome ?></td>
                <td><?= $product->sku ?></td>
                <td>R$ <?= $product->preco ?></td>
                <td><input type="number" id="product[<?=$product->id?>]" name="product[<?=$product->id?>]" step="1" min="0" max="100"
                                                          onkeypress='return event.charCode >= 48 && event.charCode <= 57' value="0"></td>
            </tr>
        <?php } ?>
        </tbody>
        <div>
            <p for="client"><b>Cliente: </b><select id="client" name="client" ></p>
            <?php foreach ($clients as $client) { ?>
                <option value="<?=$client->id?>"><?=$client->nombre?> <?=$client->apellido?></option> 
            <?php } ?>
            </select>
        </div>
        <div class="mb-3">
            <button class="btn btn-primary px-2" type="submit">Criar pedido</button>
        </div>
    </form>
    <?php 
    } else { ?>
        <div class="col-sm-12 col-xs-12">Não há produtos</div>
    <?php } ?>
</div>
<?php $this->load->view('_partials/scripts'); ?>
</body>

</html>