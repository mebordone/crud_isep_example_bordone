<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-BR">
<?php $this->load->view('_partials/head'); ?>
<body>
<?php $this->load->view('_partials/header'); ?>
<div class="container container-person mt-5 p-5">
    <?=write_message()?>
    <?php
    $action_form = '/client/save/';
    if(isset($client) && $client){
        foreach ($client as $cliente);
        $action_form = $action_form.$cliente->id ?>
        <h1>Editar Cliente: <?= $cliente->dni ?></h1>
    <?php } else { ?>
        <h1>Cadastro de Cliente</h1>
    <?php } ?>
    <form id="form_client" method="post" enctype="multipart/form-data" action="<?=site_url($action_form)?>">
        <div class="form-row">
            <div class="col-md-4 mb-3">
                <label for="nombre">Nome *</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nome" required value="<?= (isset($cliente) ? $cliente->nombre : '') ?>">
            </div>
            <div class="col-md-4 mb-3">
                <label for="apellido">Sobrenome *</label>
                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Sobrenome" required value="<?= (isset($cliente) ? $cliente->apellido : '') ?>">
            </div>
            <div class="col-md-4 mb-3">
                <label for="dni">Dni *</label>
                <input type="text" class="form-control" id="dni" name="dni" placeholder="DNI" required value="<?= (isset($cliente) ? $cliente->dni : '') ?>">
            </div>
        </div>
        <div class="form-row">
            <div class="col-md-4 mb-3">
                <label for="f_nac">Aniversário </label>
                <input type="date" class="form-control" id="f_nac" name="f_nac" placeholder="Aniversário" value="<?= (isset($cliente) ? $cliente->f_nac : '') ?>">
            </div>
            <div class="col-md-4 mb-3">
                <label for="f_nac">Provincia </label>
                <input type="text" class="form-control" id="provincia" name="provincia" placeholder="Provincia" value="<?= (isset($cliente) ? $cliente->provincia : '') ?>">
            </div>
            <div class="col-md-4 mb-3">
                <label for="email">Email </label>
                <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= (isset($cliente) ? $cliente->email : '') ?>">
            </div>
        </div>
        <button class="btn btn-primary" type="submit">Enviar</button>
        <?= (isset($cliente) ? '<a href="#" data-id="'.base_url('client/delete/'.$cliente->id).'" class="btn btn-danger delete-client" data-toggle="modal" data-target="#deleteClientModal">Excluir</a>' : '') ?>
    </form>
</div>
<?php $this->load->view('_partials/client/delete_client_confirm_modal'); ?>
<?php $this->load->view('_partials/scripts'); ?>
</body>

</html>