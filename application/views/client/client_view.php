<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="pt-BR">
<?php $this->load->view('_partials/head'); ?>
<body>
<?php $this->load->view('_partials/header'); ?>
<div class="container container-person mt-5 p-5">
    <?=write_message()?>
    <h1>Clientes</h1>
    <div class="col-md-12 mb-3">
        <div class="row">
            <a class="btn btn-primary" href="<?= base_url('client/form/') ?>">Novo Cliente</a>
        </div>
    </div>
    <table id="client_table" class="table table-striped table-bordered table-responsive-sm" style="width:100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Sobrenome</th>
            <th>dni</th>
            <th>Aniversário</th>
            <th>Provincia</th>
            <th>Email</th>
            <th>Editar</th>
            <th>Excluir</th>
        </tr>
        </thead>
        <tbody>
        <?php
        if($clients) {
            foreach ($clients as $client) { ?>
                <tr>
                    <td><?= $client->id ?></td>
                    <td><?= $client->nombre ?></td>
                    <td><?= $client->apellido ?></td>
                    <td><?= $client->dni ?></td>
                    <td><?= $client->f_nac ?></td>
                    <td><?= $client->provincia ?></td>
                    <td><?= $client->email ?></td>
                    <td><a href="<?= base_url('client/form/'.$client->id) ?>">Edit</a></td>
                    <td><a class="delete-client" href="#" data-id="<?= base_url('client/delete/'.$client->id) ?>" data-toggle="modal" data-target="#deleteClientModal">Delete</a></td>
                </tr>
            <?php }
        } else { ?>
            <td class="text-center" colspan="6">Não há clientes</td>
        <?php } ?>
        </tbody>
    </table>
</div>
<?php $this->load->view('_partials/client/delete_client_confirm_modal'); ?>
<?php $this->load->view('_partials/scripts'); ?>
</body>

</html>