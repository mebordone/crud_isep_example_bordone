<!-- jquery -->
<script src="<?= base_url('assets/js/vendor/jquery.min.js') ?>"></script>
<!-- popper -->
<script src="<?= base_url('assets/js/vendor/popper.min.js') ?>"></script>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<!-- bootstrap -->
<script src="<?= base_url('assets/js/vendor/bootstrap.min.js') ?>"></script>
<!-- datatables -->
<script src="<?= base_url('assets/js/vendor/datatables/datatables.min.js') ?>"></script>
<!-- para usar botones en datatables JS -->  
<script src="datatables/Buttons-1.7.1/js/dataTables.buttons.min.js"></script>  
<script src="datatables/JSZip-2.5.0/jszip.min.js"></script>    
<script src="datatables/pdfmake-0.1.36/pdfmake.min.js"></script>    
<script src="datatables/Buttons-1.7.1/js/buttons.html5.min.js"></script>

<!-- summernote -->
<script src="<?= base_url('assets/js/vendor/summernote-bs4.min.js') ?>"></script>
<!-- custom js-->
<script src="<?= base_url('assets/js/custom.js') ?>"></script>