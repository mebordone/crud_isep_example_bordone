-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 25-07-2021 a las 12:46:38
-- Versión del servidor: 10.5.11-MariaDB
-- Versión de PHP: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `product_order`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) CHARACTER SET utf8 NOT NULL,
  `apellido` varchar(50) CHARACTER SET utf8 NOT NULL,
  `dni` int(11) NOT NULL,
  `f_nac` date NOT NULL,
  `provincia` varchar(50) CHARACTER SET utf8 NOT NULL,
  `email` varchar(50) CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`id`, `nombre`, `apellido`, `dni`, `f_nac`, `provincia`, `email`, `status`) VALUES
(1, 'Matias', 'Bordone', 30331261, '1983-07-29', 'Córdoba', 'mebordone@gmail.com', 1),
(2, 'Luciana', 'Cometo', 33371163, '1988-02-25', 'Córdoba', 'luciana.c.scodelari@gmail.com', 1),
(3, 'Juan', 'Garcia', 1234567, '0000-00-00', '', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `cliente` int(15) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `order`
--

INSERT INTO `order` (`id`, `data`, `status`, `cliente`) VALUES
(8, '2019-07-28 21:41:42', 1, 1),
(9, '2019-07-28 22:23:42', 1, 1),
(10, '2019-07-28 23:52:10', 1, 1),
(11, '2019-07-28 23:52:15', 1, 1),
(12, '2019-07-28 23:52:19', 1, 1),
(13, '2019-07-29 00:59:07', 1, 1),
(14, '2019-07-29 00:59:34', 1, 1),
(15, '2019-07-29 00:59:41', 1, 1),
(16, '2019-07-29 01:48:39', 1, 1),
(17, '2021-07-21 10:22:00', 0, 1),
(18, '2021-07-21 11:55:41', 0, 1),
(19, '2021-07-21 11:56:42', 0, 1),
(27, '2021-07-22 11:07:25', 1, 1),
(28, '2021-07-22 11:12:15', 1, 1),
(29, '2021-07-22 11:12:24', 1, 1),
(30, '2021-07-22 11:23:07', 1, 1),
(31, '2021-07-22 11:25:36', 1, 2),
(33, '2021-07-22 14:14:28', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `sku` varchar(45) NOT NULL,
  `nome` varchar(90) NOT NULL,
  `descricao` longtext DEFAULT NULL,
  `preco` decimal(10,2) NOT NULL,
  `status` varchar(45) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `sku`, `nome`, `descricao`, `preco`, `status`) VALUES
(4, 'Sku', 'Nome', '1', '10.00', '1'),
(5, 'tee', 'tste', '<p>12</p>', '12.00', '1'),
(6, 'ss', 'form', '<p>tesete</p>', '12.00', '1'),
(7, 'a', 'a', '<p>a</p>', '1.00', '1'),
(8, 'as', 'as', '<p><b>as</b></p>', '1.00', '1'),
(9, 'botao', 'novo', '<p>sdad</p>', '123213.00', '1'),
(10, 'ex', 'ex', 'ex', '2.00', '1'),
(11, 'novos', 'novos', '<p>aaa</p>', '12.00', '1'),
(12, '100010', 'Camiseta', '<p>Camiseta vermelha</p>', '50.00', '1'),
(13, 'Gol G2 96 1.0', 'Carro', '<p>Motor CHT</p>', '5000.00', '1'),
(14, '19022', 'Tenis Adidas', '<p><b><span style=\"font-size: 36px;\">Bonitao</span></b><span style=\"font-size: 36px;\">﻿</span></p>', '212.90', '1'),
(15, '001', 'Produto 1', '<p><b>Produto - 1: </b>Descriptions<br></p>', '10.00', '1'),
(16, '002', 'Produto 2', '', '13.90', '1'),
(17, '003', 'Produto 3', '', '17.23', '1'),
(18, '004', 'Produto 4', '', '5.15', '1'),
(19, 'excluir', 'Produto 5', '', '12.00', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_order`
--

CREATE TABLE `product_order` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_qtd` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `product_order`
--

INSERT INTO `product_order` (`id`, `order_id`, `product_id`, `product_qtd`) VALUES
(2, 8, 4, 2),
(3, 8, 5, 1),
(4, 9, 12, 2),
(5, 9, 13, 1),
(6, 9, 14, 1),
(7, 10, 12, 1),
(8, 10, 13, 1),
(9, 10, 14, 1),
(10, 11, 12, 1),
(11, 12, 14, 1),
(12, 13, 15, 1),
(13, 14, 15, 2),
(14, 14, 16, 3),
(15, 14, 17, 3),
(16, 14, 18, 1),
(17, 15, 15, 1),
(18, 15, 18, 1),
(19, 16, 19, 1),
(27, 25, 4, 1),
(28, 26, 4, 1),
(29, 27, 4, 1),
(30, 29, 4, 1),
(31, 30, 4, 1),
(32, 31, 17, 1),
(33, 33, 7, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cliente` (`cliente`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `product_order`
--
ALTER TABLE `product_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_po_idx` (`order_id`),
  ADD KEY `product_po_idx` (`product_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `product_order`
--
ALTER TABLE `product_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `order_ibfk_1` FOREIGN KEY (`cliente`) REFERENCES `client` (`id`);

--
-- Filtros para la tabla `product_order`
--
ALTER TABLE `product_order`
  ADD CONSTRAINT `order_po` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `product_po` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
